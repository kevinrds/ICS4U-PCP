# SOLAR SYSTEM CONSOLE APPLICATION VERSIONING

## LATEST VERSION - VERSION #3
## LATEST STABLE VERSION - VERSION #2

### VERSION #3 - Solar System Console Application (Latest Version)
  * PROJECT: SolarSystem3.0
  * LINK: https://gitlab.com/kevinrds/ICS4U-PCP/tree/master/SolarSystem3.0
  * CODE: https://gitlab.com/kevinrds/ICS4U-PCP/blob/master/SolarSystem3.0/SolarSystem3.0/SolarSystem3.0.cpp
  * INSTALLER DATA: https://gitlab.com/kevinrds/ICS4U-PCP/tree/master/SolarSystem3.0Installation
  * INSTALLER: https://gitlab.com/kevinrds/ICS4U-PCP/blob/master/SolarSystem3.0Installation/Solar%20System%20Installer.EXE
  * UPDATES:
    * Minor
      * Hides console window on startup.
    * Major
      * Can open International Space Station live feed in browser.
      * Can track International Space Station in browser.
  * KNOWN ISSUES:
    * Problems when including SFML & Windows.h. (SHOULD BE RESOLVED)
  

### VERSION #2 - Solar System Console Application (Latest Stable Version)
  * PROJECT: SolarSystem2.0
  * LINK: https://gitlab.com/kevinrds/ICS4U-PCP/tree/master/SolarSystem2.0
  * CODE: https://gitlab.com/kevinrds/ICS4U-PCP/blob/master/SolarSystem2.0/SolarSystem2.0/SolarSystem2.0.cpp
  * INSTALLER DATA: https://gitlab.com/kevinrds/ICS4U-PCP/tree/master/SolarSystem2.0Installation
  * INSTALLER: https://gitlab.com/kevinrds/ICS4U-PCP/blob/master/SolarSystem2.0Installation/Solar%20System%20Installer.EXE
  * UPDATES:
    * Minor
      * Icon change.
      * Speed change.
      * Volume change.
    * Major
      * Play or pause motion of planets.
      * Ability to zoom in and zoom out.

### VERSION #1 - Solar System Console Application
  * PROJECT: SolarSystem
  * LINK: https://gitlab.com/kevinrds/ICS4U-PCP/tree/master/SolarSystem
  * CODE: https://gitlab.com/kevinrds/ICS4U-PCP/blob/master/SolarSystem/SolarSystem/SolarSystem.cpp
  * INSTALLER DATA: https://gitlab.com/kevinrds/ICS4U-PCP/tree/master/SolarSystemInstallation
  * INSTALLER: https://gitlab.com/kevinrds/ICS4U-PCP/blob/master/SolarSystemInstallation/SolarSystemInstaller.exe
  * FEATURES:
    * Model
      * Select type of model, speed of model, and presence of other features (background, orbit rings, labels).
    * Other
      * Title screen present (click buttons, hear music, hear button click sound effect, set volume).
      * Select and save settings to text file (saved settings present next time user opens app).
      * Select type of model, speed of model, and presence of other features (background, orbit rings, labels).
      * Obtains planet data (orbital velocity, orbital radius, diameter of planet) from text file.
      * Can return to title screen after model is viewed.

### PROGRAM DESIGN #2: C++ & SFML Console Application
  * PROJECT: win32_console_app_pre_alpha
  * LINK: https://gitlab.com/kevinrds/ICS4U-PCP/tree/master/win32_console_app_pre_alpha
  * CODE: https://gitlab.com/kevinrds/ICS4U-PCP/blob/master/win32_console_app_pre_alpha/win32_console_app_pre_alpha/win32_console_app_pre_alpha.cpp

### PRACTISE STAGE #2: C++ & SFML
  * PROJECT: console_cpp_sfml
  * LINK: https://gitlab.com/kevinrds/ICS4U-PCP/tree/master/console_cpp_sfml
  * CODE: https://gitlab.com/kevinrds/ICS4U-PCP/blob/master/console_cpp_sfml/console_cpp_sfml/console_cpp_sfml.cpp

### PROGRAM DESIGN #1: C++ & XAML UWP Application
  * PROJECT: app_pre_alpha
  * LINK: https://gitlab.com/kevinrds/ICS4U-PCP/tree/master/app_pre_alpha
  * CODE: Multiple Files - See Project Folder

### PRACTISE STAGE #1: C++
  * PROJECT: cpp_practice
  * LINK: https://gitlab.com/kevinrds/ICS4U-PCP/tree/master/cpp_practice
  * CODE: https://gitlab.com/kevinrds/ICS4U-PCP/blob/master/cpp_practice/cpp_practice/cpp_practice.cpp
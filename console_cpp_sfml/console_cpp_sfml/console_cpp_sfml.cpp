// console_cpp_sfml.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <SFML/Graphics.hpp>

int main()
{
	sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
	sf::CircleShape shape(100.f);
	shape.setFillColor(sf::Color::Green);

	sf::View view;
	view.setCenter(100.0f, 100.0f);
	view.setSize(200.0f, 200.0f);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			else if (event.type == event.MouseButtonReleased)
			{
				sf::Vector2i mouse_posp = sf::Mouse::getPosition(window);
				sf::Vector2f mouse_posc = window.mapPixelToCoords(mouse_posp);

				sf::FloatRect bounding_box1 = shape.getGlobalBounds();

				if (bounding_box1.contains(mouse_posc))
				{
					view.zoom(0.8f);
				}
			}
		}

		window.clear();
		window.draw(shape);
		window.setView(view);
		window.setFramerateLimit(60);
		window.display();
	}

	return 0;
}
// cpp_practice.cpp : Defines the entry point for the console application.
// line comment
/* block comment*/

#include "stdafx.h"
#include <iostream> // output
#include <string> // strings
using namespace std; // allows us to remove std:: in front of cout 

#define PI 3.14159
/*
int main()
{
	cout << "Hello Worlddd!!!! Mah first C++ program." << endl;

	int x = 10;
	float y;
	y = 10.10;
	cout << "My favourite number plus 10 is " << x + y << endl;

	int a = 100;
	auto b = a;
	cout << "Trying some tricks, " << a + b << endl;

	string mystring = "My first string.";
	cout << mystring << endl;

	cout << "Solution\nis\t\a" << PI * 3 << endl; //\a --> beep

	/* bool - true/false & start with is | eg ishappy = true
	char - just one character / single quotes | eg grade = 'A'
	int - integer | eg age = 39
	float - 6 decimal places | eg 3.141592
	double - up to 15 digits in length | eg 1.231231231
	void - absence of type

	const (var type) - value CANNOT BE CHANGED *//*

	int m = 5, n = 20;
	cout << (5 + m) << endl;
	cout << int(n / 10) << endl; // % modulo (remainder) | * multiplication

    return 0;
}
*/


int main() //j12016
{
	char win_loss1;
	char win_loss2;
	char win_loss3;
	char win_loss4;
	char win_loss5;
	char win_loss6;

	int win = 0;
	int loss = 0;

	cin >> win_loss1;
	cin >> win_loss2;
	cin >> win_loss3;
	cin >> win_loss4;
	cin >> win_loss5;
	cin >> win_loss6;

	if (win_loss1 == 'W')
		win += 1;
	if (win_loss2 == 'W')
		win += 1;
	if (win_loss3 == 'W')
		win += 1;
	if (win_loss4 == 'W')
		win += 1;
	if (win_loss5 == 'W')
		win += 1;
	if (win_loss6 == 'W')
		win += 1;

	if ((win == 5) || (win == 6))
		cout << "1" << endl;
	else if ((win == 3) || (win == 4))
		cout << "2" << endl;
	else if ((win == 1) || (win == 2))
		cout << "3" << endl;
	else
		cout << "-1" << endl;

}

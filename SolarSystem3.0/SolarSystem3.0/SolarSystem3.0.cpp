// SolarSystem3.0.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <Windows.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <cmath>
#define PI 3.14159265

class IntroScreen
{
	public:
		void intro()
		{
			// Open and read from settings text file
			std::string line;
			std::ifstream settings_file("settings.txt");

			std::vector<std::string> settings_data;

			if (settings_file.is_open())
			{
				while (std::getline(settings_file, line))
				{
					settings_data.push_back(line);
				}
				settings_file.close();
			}

			// Create SFML window
			sf::RenderWindow window;
			window.create(sf::VideoMode(1000, 650), "Solar System", sf::Style::Titlebar | sf::Style::Close);
			window.setPosition(sf::Vector2i(10, 10));

			// Load icon image
			sf::Image icon;
			icon.loadFromFile("icon.png");
			window.setIcon(512, 512, icon.getPixelsPtr());

			// Open audio files
			sf::Music intro_audio, button_click;

			intro_audio.openFromFile("intro_audio.ogg");
			intro_audio.setLoop(true);

			button_click.openFromFile("button_click.ogg");

			// Create stars for background
			std::vector<sf::CircleShape> starry_sky;
			for (int i = 0; i <= 3000; i += 1)
			{
				sf::CircleShape star(0.5);
				star.setFillColor(sf::Color::White);
				star.setPosition(rand() % 1001, rand() % 651);
				starry_sky.push_back(star);
			}

			// Title screen rectangle boxes
			sf::RectangleShape box_text1(sf::Vector2f(125, 55));
			box_text1.setFillColor(sf::Color::Red);
			box_text1.setPosition(430, 345);

			sf::RectangleShape box_text2(sf::Vector2f(195, 55));
			box_text2.setFillColor(sf::Color::Red);
			box_text2.setPosition(395, 435);

			sf::RectangleShape box_text3(sf::Vector2f(100, 55));
			box_text3.setFillColor(sf::Color::Red);
			box_text3.setPosition(445, 525);

			sf::RectangleShape iss_live_button(sf::Vector2f(185, 55));
			iss_live_button.setFillColor(sf::Color::Red);
			iss_live_button.setPosition(115, 435);

			sf::RectangleShape track_iss_button(sf::Vector2f(215, 55));
			track_iss_button.setFillColor(sf::Color::Red);
			track_iss_button.setPosition(690, 435);

			// Settings screen rectangle boxes
			sf::RectangleShape box_text4(sf::Vector2f(40, 40));
			box_text4.setFillColor(sf::Color::Red);
			box_text4.setPosition(115, 145);

			sf::RectangleShape box_text5(sf::Vector2f(40, 40));
			box_text5.setFillColor(sf::Color::Red);
			box_text5.setPosition(115, 200);

			sf::RectangleShape box_text6(sf::Vector2f(40, 40));
			box_text6.setFillColor(sf::Color::Red);
			box_text6.setPosition(625, 145);

			sf::RectangleShape box_text7(sf::Vector2f(40, 40));
			box_text7.setFillColor(sf::Color::Red);
			box_text7.setPosition(625, 200);

			sf::RectangleShape box_text8(sf::Vector2f(40, 40));
			box_text8.setFillColor(sf::Color::Red);
			box_text8.setPosition(350, 405);

			sf::RectangleShape box_text9(sf::Vector2f(40, 40));
			box_text9.setFillColor(sf::Color::Red);
			box_text9.setPosition(350, 460);

			sf::RectangleShape box_text10(sf::Vector2f(40, 40));
			box_text10.setFillColor(sf::Color::Red);
			box_text10.setPosition(350, 515);

			sf::RectangleShape box_text11(sf::Vector2f(130, 55));
			box_text11.setFillColor(sf::Color::Red);
			box_text11.setPosition(860, 585);

			// Initialize check mark texture
			sf::Texture check_texture;
			check_texture.loadFromFile("cm.png");
			check_texture.setSmooth(true);

			// Create all settings screen check mark sprites
			sf::RectangleShape check1(sf::Vector2f(40, 40));
			check1.setTexture(&check_texture);
			check1.setPosition(115, 145);

			sf::RectangleShape check2(sf::Vector2f(40, 40));
			check2.setTexture(&check_texture);
			check2.setPosition(115, 200);

			sf::RectangleShape check3(sf::Vector2f(40, 40));
			check3.setTexture(&check_texture);
			check3.setPosition(625, 145);

			sf::RectangleShape check4(sf::Vector2f(40, 40));
			check4.setTexture(&check_texture);
			check4.setPosition(625, 200);

			sf::RectangleShape check5(sf::Vector2f(40, 40));
			check5.setTexture(&check_texture);
			check5.setPosition(350, 405);

			sf::RectangleShape check6(sf::Vector2f(40, 40));
			check6.setTexture(&check_texture);
			check6.setPosition(350, 460);

			sf::RectangleShape check7(sf::Vector2f(40, 40));
			check7.setTexture(&check_texture);
			check7.setPosition(350, 515);

			// Initialize audio icon textures
			sf::Texture hv_texture;
			hv_texture.loadFromFile("high_volume.png");
			hv_texture.setSmooth(true);

			sf::Texture lv_texture;
			lv_texture.loadFromFile("low_volume.png");
			lv_texture.setSmooth(true);

			sf::Texture mute_texture;
			mute_texture.loadFromFile("mute.png");
			mute_texture.setSmooth(true);

			// Create all audio icons
			sf::RectangleShape hv(sf::Vector2f(40, 40));
			hv.setTexture(&hv_texture);
			hv.setPosition(950, 600);

			sf::RectangleShape lv(sf::Vector2f(40, 40));
			lv.setTexture(&lv_texture);
			lv.setPosition(950, 600);

			sf::RectangleShape mute(sf::Vector2f(40, 40));
			mute.setTexture(&mute_texture);
			mute.setPosition(950, 600);

			// Initialize game title font
			sf::Font title_font;
			title_font.loadFromFile("title.ttf");

			// Initialize game title text variables
			sf::Text title_text1, title_text2;

			// Create game title text
			title_text1.setFont(title_font);
			title_text1.setString("SOLAR");
			title_text1.setCharacterSize(125);
			title_text1.setColor(sf::Color::White);
			title_text1.setPosition(225, 25);

			title_text2.setFont(title_font);
			title_text2.setString("SYSTEM");
			title_text2.setCharacterSize(125);
			title_text2.setColor(sf::Color::White);
			title_text2.setPosition(160, 150);

			// Initialize font for other text in game
			sf::Font reg_font;
			reg_font.loadFromFile("reg.ttf");

			// Initialize other text variables
			sf::Text reg_text1, reg_text2, reg_text3, iss_live_text, track_iss_text, reg_text4, reg_text5, reg_text6, reg_text7, reg_text8, reg_text9, reg_text10, reg_text11, reg_text12, reg_text13, reg_text14;

			// Title screen other text
			reg_text1.setFont(reg_font);
			reg_text1.setString("Start");
			reg_text1.setCharacterSize(40);
			reg_text1.setColor(sf::Color::White);
			reg_text1.setPosition(440, 345);

			reg_text2.setFont(reg_font);
			reg_text2.setString("Settings");
			reg_text2.setCharacterSize(40);
			reg_text2.setColor(sf::Color::White);
			reg_text2.setPosition(405, 435);

			reg_text3.setFont(reg_font);
			reg_text3.setString("Exit");
			reg_text3.setCharacterSize(40);
			reg_text3.setColor(sf::Color::White);
			reg_text3.setPosition(455, 525);

			iss_live_text.setFont(reg_font);
			iss_live_text.setString("ISS Live");
			iss_live_text.setCharacterSize(40);
			iss_live_text.setColor(sf::Color::White);
			iss_live_text.setPosition(125, 435);

			track_iss_text.setFont(reg_font);
			track_iss_text.setString("Track ISS");
			track_iss_text.setCharacterSize(40);
			track_iss_text.setColor(sf::Color::White);
			track_iss_text.setPosition(700, 435);

			// Settings screen other text
			reg_text4.setFont(reg_font);
			reg_text4.setString("MODEL");
			reg_text4.setCharacterSize(60);
			reg_text4.setColor(sf::Color::White);
			reg_text4.setPosition(175, 65);

			reg_text5.setFont(reg_font);
			reg_text5.setString("Inner Planets");
			reg_text5.setCharacterSize(40);
			reg_text5.setColor(sf::Color::White);
			reg_text5.setPosition(165, 140);

			reg_text6.setFont(reg_font);
			reg_text6.setString("Outer Planets");
			reg_text6.setCharacterSize(40);
			reg_text6.setColor(sf::Color::White);
			reg_text6.setPosition(165, 195);

			reg_text7.setFont(reg_font);
			reg_text7.setString("SPEED");
			reg_text7.setCharacterSize(60);
			reg_text7.setColor(sf::Color::White);
			reg_text7.setPosition(600, 65);

			reg_text8.setFont(reg_font);
			reg_text8.setString("Slow");
			reg_text8.setCharacterSize(40);
			reg_text8.setColor(sf::Color::White);
			reg_text8.setPosition(675, 140);

			reg_text9.setFont(reg_font);
			reg_text9.setString("Fast");
			reg_text9.setCharacterSize(40);
			reg_text9.setColor(sf::Color::White);
			reg_text9.setPosition(675, 195);

			reg_text10.setFont(reg_font);
			reg_text10.setString("OTHER");
			reg_text10.setCharacterSize(60);
			reg_text10.setColor(sf::Color::White);
			reg_text10.setPosition(400, 325);

			reg_text11.setFont(reg_font);
			reg_text11.setString("Background");
			reg_text11.setCharacterSize(40);
			reg_text11.setColor(sf::Color::White);
			reg_text11.setPosition(400, 400);

			reg_text12.setFont(reg_font);
			reg_text12.setString("Orbit Rings");
			reg_text12.setCharacterSize(40);
			reg_text12.setColor(sf::Color::White);
			reg_text12.setPosition(400, 455);

			reg_text13.setFont(reg_font);
			reg_text13.setString("Labels");
			reg_text13.setCharacterSize(40);
			reg_text13.setColor(sf::Color::White);
			reg_text13.setPosition(400, 510);

			reg_text14.setFont(reg_font);
			reg_text14.setString("Save");
			reg_text14.setCharacterSize(40);
			reg_text14.setColor(sf::Color::White);
			reg_text14.setPosition(870, 585);

			// Initialize setting change (bool) variable (user wants to access settings changes to true)
			bool setting_change = false;

			// From settings text file assign data from text file to variables
			std::string inner_planet = settings_data[0];
			std::string outer_planet = settings_data[1];
			std::string slow = settings_data[2];
			std::string fast = settings_data[3];
			std::string background = settings_data[4];
			std::string orbit_rings = settings_data[5];
			std::string labels = settings_data[6];
			std::string audio_status = settings_data[7];

			// Check what audio status is listed in settings text file and change volume based upon that
			if (audio_status == "hv")
			{
				intro_audio.setVolume(100);
				button_click.setVolume(5);

				intro_audio.play();
			}

			else if (audio_status == "lv")
			{
				intro_audio.setVolume(5);
				button_click.setVolume(5);

				intro_audio.play();
			}

			else if (audio_status == "mute")
			{
				intro_audio.setVolume(0);
				button_click.setVolume(0);

				intro_audio.play();
			}

			// Check if SFML window is open
			while (window.isOpen())
			{
				sf::Event event;

				while (window.pollEvent(event))
				{
					// Check if user wants to quit
					if (event.type == sf::Event::Closed)
					{
						intro_audio.stop();
						window.close();
						exit(EXIT_SUCCESS);
					}

					// Check if mouse button has been clicked
					else if (event.type == event.MouseButtonReleased)
					{
						// Get location of mouse right after click
						sf::Vector2i mouse_posp = sf::Mouse::getPosition(window);
						sf::Vector2f mouse_posc = window.mapPixelToCoords(mouse_posp);

						// Create bounding boxes for buttons
						sf::FloatRect bounding_box1 = box_text1.getGlobalBounds();
						sf::FloatRect bounding_box2 = box_text2.getGlobalBounds();
						sf::FloatRect bounding_box3 = box_text3.getGlobalBounds();
						sf::FloatRect bounding_box4 = hv.getGlobalBounds();
						sf::FloatRect bounding_box5 = lv.getGlobalBounds();
						sf::FloatRect bounding_box6 = mute.getGlobalBounds();
						sf::FloatRect bounding_box7 = iss_live_button.getGlobalBounds();
						sf::FloatRect bounding_box8 = track_iss_button.getGlobalBounds();

						// Check if button clicked
						// If button clicked, play button effect, and perform task (move to another window or close screen)
						if (bounding_box1.contains(mouse_posc))
						{
							button_click.play();
							while (button_click.getStatus() == button_click.Playing)
							{
								;
							}

							return;
						}

						else if (bounding_box2.contains(mouse_posc))
						{
							button_click.play();
							while (button_click.getStatus() == button_click.Playing)
							{
								;
							}

							setting_change = true;
						}

						else if (bounding_box3.contains(mouse_posc))
						{
							button_click.play();
							while (button_click.getStatus() == button_click.Playing)
							{
								;
							}

							intro_audio.stop();
							window.close();
							exit(EXIT_SUCCESS);
						}

						// If audio icon clicked, change audio status
						else if ((audio_status == "hv") && (bounding_box4.contains(mouse_posc)))
						{
							audio_status = "mute";
							intro_audio.setVolume(0);
							button_click.setVolume(0);
						}

						else if ((audio_status == "lv") && (bounding_box5.contains(mouse_posc)))
						{
							audio_status = "hv";
							intro_audio.setVolume(100);
							button_click.setVolume(100);
						}

						else if ((audio_status == "mute") && (bounding_box6.contains(mouse_posc)))
						{
							audio_status = "lv";
							intro_audio.setVolume(5);
							button_click.setVolume(5);
						}

						// If user wants to see ISS live feed or track ISS in real time, open link in default web browser.
						else if (bounding_box7.contains(mouse_posc))
						{
							button_click.play();
							while (button_click.getStatus() == button_click.Playing)
							{
								;
							}

							ShellExecuteA(NULL, "open", "http://www.ustream.tv/embed/17074538?html5ui?v=3&amp;controls=false&autoplay=true", NULL, NULL, SW_SHOWNORMAL);
						}

						else if (bounding_box8.contains(mouse_posc))
						{
							button_click.play();
							while (button_click.getStatus() == button_click.Playing)
							{
								;
							}

							ShellExecuteA(NULL, "open", "http://www.lizard-tail.com/isana/tracking/", NULL, NULL, SW_SHOWNORMAL);
						}

						// Write new settings to text file
						std::ofstream settings_file("settings.txt");
						settings_file << inner_planet << "\n";
						settings_file << outer_planet << "\n";
						settings_file << slow << "\n";
						settings_file << fast << "\n";
						settings_file << background << "\n";
						settings_file << orbit_rings << "\n";
						settings_file << labels << "\n";
						settings_file << audio_status << "\n";
						settings_file.close();
					}
				}

				// Switch to settings screen if user wants to change settings
				while (setting_change == true)
				{
					sf::Event event;

					while (window.pollEvent(event))
					{
						// Check if user wants to quit
						if (event.type == sf::Event::Closed)
						{
							intro_audio.stop();
							window.close();
							exit(EXIT_SUCCESS);
						}

						// Check if mouse button has been clicked
						else if (event.type == event.MouseButtonReleased)
						{
							// Get location of mouse right after click
							sf::Vector2i mouse_posp = sf::Mouse::getPosition(window);
							sf::Vector2f mouse_posc = window.mapPixelToCoords(mouse_posp);

							// Create bounding boxes for each button
							sf::FloatRect bounding_box1 = box_text4.getGlobalBounds();
							sf::FloatRect bounding_box2 = box_text5.getGlobalBounds();
							sf::FloatRect bounding_box3 = box_text6.getGlobalBounds();
							sf::FloatRect bounding_box4 = box_text7.getGlobalBounds();
							sf::FloatRect bounding_box5 = box_text8.getGlobalBounds();
							sf::FloatRect bounding_box6 = box_text9.getGlobalBounds();
							sf::FloatRect bounding_box7 = box_text10.getGlobalBounds();
							sf::FloatRect bounding_box8 = box_text11.getGlobalBounds();

							// Check if button has been clicked, if clicked perform a certain task
							if (bounding_box1.contains(mouse_posc))
							{
								if (inner_planet == "true")
								{
									inner_planet = "false";
									outer_planet = "true";
								}

								else if (inner_planet == "false")
								{
									inner_planet = "true";
									outer_planet = "false";
								}
							}

							else if (bounding_box2.contains(mouse_posc))
							{
								if (outer_planet == "true")
								{
									outer_planet = "false";
									inner_planet = "true";
								}

								else if (outer_planet == "false")
								{
									outer_planet = "true";
									inner_planet = "false";
								}
							}

							else if (bounding_box3.contains(mouse_posc))
							{
								if (slow == "true")
								{
									slow = "false";
									fast = "true";
								}

								else if (slow == "false")
								{
									slow = "true";
									fast = "false";
								}
							}

							else if (bounding_box4.contains(mouse_posc))
							{
								if (fast == "true")
								{
									fast = "false";
									slow = "true";
								}

								else if (fast == "false")
								{
									fast = "true";
									slow = "false";
								}
							}

							else if (bounding_box5.contains(mouse_posc))
							{
								if (background == "true")
								{
									background = "false";
								}

								else if (background == "false")
								{
									background = "true";
								}
							}

							else if (bounding_box6.contains(mouse_posc))
							{
								if (orbit_rings == "true")
								{
									orbit_rings = "false";
								}

								else if (orbit_rings == "false")
								{
									orbit_rings = "true";
								}
							}

							else if (bounding_box7.contains(mouse_posc))
							{
								if (labels == "true")
								{
									labels = "false";
								}

								else if (labels == "false")
								{
									labels = "true";
								}
							}

							// Write new settings to text file
							std::ofstream settings_file("settings.txt");
							settings_file << inner_planet << "\n";
							settings_file << outer_planet << "\n";
							settings_file << slow << "\n";
							settings_file << fast << "\n";
							settings_file << background << "\n";
							settings_file << orbit_rings << "\n";
							settings_file << labels << "\n";
							settings_file << audio_status << "\n";
							settings_file.close();

							// Check if user wants to return to title screen
							if (bounding_box8.contains(mouse_posc))
							{
								button_click.play();
								while (button_click.getStatus() == button_click.Playing)
								{
									;
								}

								setting_change = false;
							}
						}
					}

					window.clear();

					// Draw background
					for (int i = 0; i <= 3000; i += 1)
					{
						window.draw(starry_sky[i]);
					}

					// Draw buttons
					window.draw(box_text4);
					window.draw(box_text5);
					window.draw(box_text6);
					window.draw(box_text7);
					window.draw(box_text8);
					window.draw(box_text9);
					window.draw(box_text10);
					window.draw(box_text11);

					// Check which options have been selected
					if (inner_planet == "true")
					{
						window.draw(check1);
					}

					if (outer_planet == "true")
					{
						window.draw(check2);
					}

					if (slow == "true")
					{
						window.draw(check3);
					}

					if (fast == "true")
					{
						window.draw(check4);
					}

					if (background == "true")
					{
						window.draw(check5);
					}

					if (orbit_rings == "true")
					{
						window.draw(check6);
					}

					if (labels == "true")
					{
						window.draw(check7);
					}

					// Draw text to screen
					window.draw(reg_text4);
					window.draw(reg_text5);
					window.draw(reg_text6);
					window.draw(reg_text7);
					window.draw(reg_text8);
					window.draw(reg_text9);
					window.draw(reg_text10);
					window.draw(reg_text11);
					window.draw(reg_text12);
					window.draw(reg_text13);
					window.draw(reg_text14);

					window.setFramerateLimit(60);
					window.display();

					// If user wants to leave settings screen break while loop
					if (setting_change == false)
					{
						break;
					}
				}

				window.clear();

				// Draw background
				for (int i = 0; i <= 3000; i += 1)
				{
					window.draw(starry_sky[i]);
				}

				// Draw buttons
				window.draw(box_text1);
				window.draw(box_text2);
				window.draw(box_text3);
				window.draw(iss_live_button);
				window.draw(track_iss_button);

				// Draw title text and button text
				window.draw(title_text1);
				window.draw(title_text2);
				window.draw(reg_text1);
				window.draw(reg_text2);
				window.draw(reg_text3);
				window.draw(iss_live_text);
				window.draw(track_iss_text);

				// Draw audio icons
				if (audio_status == "hv")
				{
					window.draw(hv);
				}

				else if (audio_status == "lv")
				{
					window.draw(lv);
				}

				else if (audio_status == "mute")
				{
					window.draw(mute);
				}

				window.setFramerateLimit(60);
				window.display();
			}
		}
};

class GetData
{
	friend class SSModel;

	private:
		float get_or(std::string planet)
		{
			// Open and read orbital radius data from text file
			std::string line;
			std::ifstream data_file("ordata.txt");

			std::vector<float> or_data;

			if (data_file.is_open())
			{
				while (std::getline(data_file, line))
				{
					float value = atoi(line.c_str());
					or_data.push_back(value);
				}
				data_file.close();
			}

			float orbital_radius, orbital_radius_scaled;

			// Given planet name find orbital radius
			if (planet == "mercury")
			{
				orbital_radius = or_data[0];
			}

			else if (planet == "venus")
			{
				orbital_radius = or_data[1];
			}

			else if (planet == "earth")
			{
				orbital_radius = or_data[2];
			}

			else if (planet == "mars")
			{
				orbital_radius = or_data[3];
			}

			else if (planet == "jupiter")
			{
				orbital_radius = or_data[4];
			}

			else if (planet == "saturn")
			{
				orbital_radius = or_data[5];
			}

			else if (planet == "uranus")
			{
				orbital_radius = or_data[6];
			}

			else if (planet == "neptune")
			{
				orbital_radius = or_data[7];
			}

			// Check if inner planets or outer planets and scale orbital radius
			if (planet == "mercury" || planet == "venus" || planet == "earth" || planet == "mars")
			{
				orbital_radius_scaled = orbital_radius / (std::ceil(or_data[3]) / 310);
				return orbital_radius_scaled;
			}

			else if (planet == "jupiter" || planet == "saturn" || planet == "uranus" || planet == "neptune")
			{
				orbital_radius_scaled = orbital_radius / (std::ceil(or_data[7]) / 310);
				return orbital_radius_scaled;
			}
		}

		float get_ov(std::string planet, std::string speed)
		{
			// Open and read orbital velocity data from text file
			std::string line;
			std::ifstream data_file("ovdata.txt");

			std::vector<float> ov_data;

			if (data_file.is_open())
			{
				while (std::getline(data_file, line))
				{
					float value = atoi(line.c_str());
					ov_data.push_back(value);
				}
				data_file.close();
			}

			float orbital_velocity, orbital_velocity_scaled;

			// Given planet name, find corresponding orbital velocity
			if (planet == "mercury")
			{
				orbital_velocity = ov_data[0];
			}

			else if (planet == "venus")
			{
				orbital_velocity = ov_data[1];
			}

			else if (planet == "earth")
			{
				orbital_velocity = ov_data[2];
			}

			else if (planet == "mars")
			{
				orbital_velocity = ov_data[3];
			}

			else if (planet == "jupiter")
			{
				orbital_velocity = ov_data[4];
			}

			else if (planet == "saturn")
			{
				orbital_velocity = ov_data[5];
			}

			else if (planet == "uranus")
			{
				orbital_velocity = ov_data[6];
			}

			else if (planet == "neptune")
			{
				orbital_velocity = ov_data[7];
			}

			int scale_factor;

			// Check whether user wants slow or fast speed, and assign corresponding scale factor
			if (speed == "slow")
			{
				scale_factor = 2;
			}

			else if (speed == "fast")
			{
				scale_factor = 4;
			}

			// Check whether inner or outer planets and scale orbital velocity
			if (planet == "mercury" || planet == "venus" || planet == "earth" || planet == "mars")
			{

				orbital_velocity_scaled = (orbital_velocity / ov_data[0]) * scale_factor;
				return orbital_velocity_scaled;
			}

			else if (planet == "jupiter" || planet == "saturn" || planet == "uranus" || planet == "neptune")
			{
				orbital_velocity_scaled = (orbital_velocity / ov_data[4]) * scale_factor;
				return orbital_velocity_scaled;
			}
		}

		float get_d(std::string planet)
		{
			// Open and read diameter of planet data from text file
			std::string line;
			std::ifstream data_file("ddata.txt");

			std::vector<float> d_data;

			if (data_file.is_open())
			{
				while (std::getline(data_file, line))
				{
					float value = atoi(line.c_str());
					d_data.push_back(value);
				}
				data_file.close();
			}

			float diameter, diameter_scaled;

			// Given name of planet, find corresponding diameter
			if (planet == "mercury")
			{
				diameter = d_data[0];
			}

			else if (planet == "venus")
			{
				diameter = d_data[1];
			}

			else if (planet == "earth")
			{
				diameter = d_data[2];
			}

			else if (planet == "mars")
			{
				diameter = d_data[3];
			}

			else if (planet == "jupiter")
			{
				diameter = d_data[4];
			}

			else if (planet == "saturn")
			{
				diameter = d_data[5];
			}

			else if (planet == "uranus")
			{
				diameter = d_data[6];
			}

			else if (planet == "neptune")
			{
				diameter = d_data[7];
			}

			// Check if inner or outer planet, find smallest diameter and use that to scale diameter of planet
			if (planet == "mercury" || planet == "venus" || planet == "earth" || planet == "mars")
			{
				float smallest_diameter = 0;

				for (int i = 0; i <= 3; i += 1)
				{
					if (i == 0)
					{
						smallest_diameter = d_data[0];
					}
					else if (d_data[i] < smallest_diameter)
					{
						smallest_diameter = d_data[i];
					}
				}

				diameter_scaled = (diameter / smallest_diameter) * 15;
				return diameter_scaled;
			}

			else if (planet == "jupiter" || planet == "saturn" || planet == "uranus" || planet == "neptune")
			{
				float smallest_diameter = 0;

				for (int i = 4; i <= 7; i += 1)
				{
					if (i == 4)
					{
						smallest_diameter = d_data[4];
					}
					else if (d_data[i] < smallest_diameter)
					{
						smallest_diameter = d_data[i];
					}
				}

				diameter_scaled = (diameter / smallest_diameter) * 15;
				return diameter_scaled;
			}
		}
};

class SSModel
{
	public:
		void iss_model(std::string slow, std::string fast, std::string background, std::string orbit_rings, std::string labels, std::string audio_status)
		{
			GetData data;

			// Get orbital radius data
			float or_mercury, or_venus, or_earth, or_mars;
			or_mercury = data.get_or("mercury");
			or_venus = data.get_or("venus");
			or_earth = data.get_or("earth");
			or_mars = data.get_or("mars");

			// Get speed requested by user
			std::string speed;
			if (slow == "true")
			{
				speed = "slow";
			}

			else if (fast == "true")
			{
				speed = "fast";
			}

			// Get orbital velocity data
			float ov_mercury, ov_venus, ov_earth, ov_mars;
			ov_mercury = data.get_ov("mercury", speed);
			ov_venus = data.get_ov("venus", speed);
			ov_earth = data.get_ov("earth", speed);
			ov_mars = data.get_ov("mars", speed);

			// Get planet diameter data
			float d_mercury, d_venus, d_earth, d_mars;
			d_mercury = data.get_d("mercury");
			d_venus = data.get_d("venus");
			d_earth = data.get_d("earth");
			d_mars = data.get_d("mars");

			// Initialize vectors containing x and y coordinates of planets in unit circle
			std::vector<float> mer_all_xvals, ven_all_xvals, ear_all_xvals, mar_all_xvals;
			std::vector<float> mer_all_yvals, ven_all_yvals, ear_all_yvals, mar_all_yvals;

			// Using orbital velocity get coordinates of points on unit circle
			for (float theta = 0; theta <= 360; theta += ov_mercury)
			{
				float xval = (or_mercury * cos(theta * PI / 180.0)) + 490;
				mer_all_xvals.push_back(xval);

				float yval = ((or_mercury * sin(theta * PI / 180.0)) - 315) * -1;
				mer_all_yvals.push_back(yval);
			}

			for (float theta = 0; theta <= 360; theta += ov_venus)
			{
				float xval = (or_venus * cos(theta * PI / 180.0)) + 490;
				ven_all_xvals.push_back(xval);

				float yval = ((or_venus * sin(theta * PI / 180.0)) - 315) * -1;
				ven_all_yvals.push_back(yval);
			}

			for (float theta = 0; theta <= 360; theta += ov_earth)
			{
				float xval = (or_earth * cos(theta * PI / 180.0)) + 490;
				ear_all_xvals.push_back(xval);

				float yval = ((or_earth * sin(theta * PI / 180.0)) - 315) * -1;
				ear_all_yvals.push_back(yval);
			}

			for (float theta = 0; theta <= 360; theta += ov_mars)
			{
				float xval = (or_mars * cos(theta * PI / 180.0)) + 490;
				mar_all_xvals.push_back(xval);

				float yval = ((or_mars * sin(theta * PI / 180.0)) - 315) * -1;
				mar_all_yvals.push_back(yval);
			}

			// Create SFML window
			sf::RenderWindow window;
			window.create(sf::VideoMode(1000, 650), "Inner Solar System", sf::Style::Titlebar | sf::Style::Close);
			window.setPosition(sf::Vector2i(10, 10));

			// Set default view
			sf::View view;
			view.setCenter(500.0f, 325.0f);
			view.setSize(1000.0f, 650.0f);

			// Load icon image
			sf::Image icon;
			icon.loadFromFile("icon.png");
			window.setIcon(512, 512, icon.getPixelsPtr());

			// Open audio files
			sf::Music button_click;
			button_click.openFromFile("button_click.ogg");

			// Create background
			std::vector<sf::CircleShape> starry_sky;
			for (int i = 0; i <= 3000; i += 1)
			{
				sf::CircleShape star(0.5);
				star.setFillColor(sf::Color::White);
				star.setPosition(rand() % 1001, rand() % 651);
				starry_sky.push_back(star);
			}

			// Create orbit rings
			sf::CircleShape mer_orbit(or_mercury);
			mer_orbit.setFillColor(sf::Color::Transparent);
			mer_orbit.setOutlineThickness(2);
			mer_orbit.setOutlineColor(sf::Color::Magenta);
			mer_orbit.setPosition(420, 245);

			sf::CircleShape ven_orbit(or_venus);
			ven_orbit.setFillColor(sf::Color::Transparent);
			ven_orbit.setOutlineThickness(2);
			ven_orbit.setOutlineColor(sf::Color::Cyan);
			ven_orbit.setPosition(360, 185);

			sf::CircleShape ear_orbit(or_earth);
			ear_orbit.setFillColor(sf::Color::Transparent);
			ear_orbit.setOutlineThickness(2);
			ear_orbit.setOutlineColor(sf::Color::Green);
			ear_orbit.setPosition(310, 135);

			sf::CircleShape mar_orbit(or_mars);
			mar_orbit.setFillColor(sf::Color::Transparent);
			mar_orbit.setOutlineThickness(2);
			mar_orbit.setOutlineColor(sf::Color::Red);
			mar_orbit.setPosition(190, 15);

			// Create return and exit buttons
			sf::RectangleShape return_button(sf::Vector2f(105, 35));
			return_button.setFillColor(sf::Color::Red);
			return_button.setPosition(10, 605);

			sf::RectangleShape exit_button(sf::Vector2f(65, 35));
			exit_button.setFillColor(sf::Color::Red);
			exit_button.setPosition(925, 605);

			// Initialize play and pause icon textures
			sf::Texture play_texture;
			play_texture.loadFromFile("play.png");
			play_texture.setSmooth(true);

			sf::Texture pause_texture;
			pause_texture.loadFromFile("pause.png");
			pause_texture.setSmooth(true);

			// Create all play and pause icons
			sf::RectangleShape play(sf::Vector2f(40, 40));
			play.setTexture(&play_texture);
			play.setPosition(10, 10);

			sf::RectangleShape pause(sf::Vector2f(40, 40));
			pause.setTexture(&pause_texture);
			pause.setPosition(10, 10);

			// Initialize texture of planets			
			sf::Texture sun_texture;
			sun_texture.loadFromFile("sun.png");
			sun_texture.setSmooth(true);

			sf::Texture mer_texture;
			mer_texture.loadFromFile("mercury.png");
			mer_texture.setSmooth(true);

			sf::Texture ven_texture;
			ven_texture.loadFromFile("venus.png");
			ven_texture.setSmooth(true);

			sf::Texture ear_texture;
			ear_texture.loadFromFile("earth.png");
			ear_texture.setSmooth(true);

			sf::Texture mar_texture;
			mar_texture.loadFromFile("mars.png");
			mar_texture.setSmooth(true);

			// Create planet sprites for legend
			sf::CircleShape sunl(15);
			sunl.setTexture(&sun_texture);
			sunl.setPosition(825, 20);

			sf::CircleShape merl(15);
			merl.setTexture(&mer_texture);
			merl.setPosition(825, 60);

			sf::CircleShape venl(15);
			venl.setTexture(&ven_texture);
			venl.setPosition(825, 100);

			sf::CircleShape earl(15);
			earl.setTexture(&ear_texture);
			earl.setPosition(825, 140);

			sf::CircleShape marl(15);
			marl.setTexture(&mar_texture);
			marl.setPosition(825, 180);

			// Create planet sprites for model
			sf::CircleShape sun(35);
			sun.setTexture(&sun_texture);
			sun.setPosition(465, 295);

			sf::CircleShape mercury(d_mercury / 2);
			mercury.setTexture(&mer_texture);
			mercury.setPosition(0, 0);

			sf::CircleShape venus(d_venus / 2);
			venus.setTexture(&ven_texture);
			venus.setPosition(0, 0);

			sf::CircleShape earth(d_earth / 2);
			earth.setTexture(&ear_texture);
			earth.setPosition(0, 0);

			sf::CircleShape mars(d_mars / 2);
			mars.setTexture(&mar_texture);
			mars.setPosition(0, 0);

			// Initialize font for text
			sf::Font reg_font;
			reg_font.loadFromFile("reg.ttf");

			// Initialize text variables
			sf::Text return_text, exit_text, tip1, tip2, reg_text1, reg_text2, reg_text3, reg_text4, reg_text5;

			// Create return and exit button text
			return_text.setFont(reg_font);
			return_text.setString("Return");
			return_text.setCharacterSize(25);
			return_text.setColor(sf::Color::White);
			return_text.setPosition(15, 605);

			exit_text.setFont(reg_font);
			exit_text.setString("Exit");
			exit_text.setCharacterSize(25);
			exit_text.setColor(sf::Color::White);
			exit_text.setPosition(932, 605);

			// Some tips
			tip1.setFont(reg_font);
			tip1.setString("Press I to zoom in");
			tip1.setCharacterSize(15);
			tip1.setColor(sf::Color::White);
			tip1.setPosition(15, 50);

			tip2.setFont(reg_font);
			tip2.setString("Press O to zoom out");
			tip2.setCharacterSize(15);
			tip2.setColor(sf::Color::White);
			tip2.setPosition(15, 65);

			// Create legend text
			reg_text1.setFont(reg_font);
			reg_text1.setString("Sun");
			reg_text1.setCharacterSize(25);
			reg_text1.setColor(sf::Color::White);
			reg_text1.setPosition(860, 20);

			reg_text2.setFont(reg_font);
			reg_text2.setString("Mercury");
			reg_text2.setCharacterSize(25);
			reg_text2.setColor(sf::Color::White);
			reg_text2.setPosition(860, 60);

			reg_text3.setFont(reg_font);
			reg_text3.setString("Venus");
			reg_text3.setCharacterSize(25);
			reg_text3.setColor(sf::Color::White);
			reg_text3.setPosition(860, 100);

			reg_text4.setFont(reg_font);
			reg_text4.setString("Earth");
			reg_text4.setCharacterSize(25);
			reg_text4.setColor(sf::Color::White);
			reg_text4.setPosition(860, 140);

			reg_text5.setFont(reg_font);
			reg_text5.setString("Mars");
			reg_text5.setCharacterSize(25);
			reg_text5.setColor(sf::Color::White);
			reg_text5.setPosition(860, 180);

			// Check current audio status and set volume
			if (audio_status == "hv")
			{
				button_click.setVolume(5);
			}

			else if (audio_status == "lv")
			{
				button_click.setVolume(5);
			}

			else if (audio_status == "mute")
			{
				button_click.setVolume(0);
			}

			// Initialize planet position variables
			int mer_pos = 0;
			int ven_pos = 0;
			int ear_pos = 0;
			int mar_pos = 0;

			// Initialize motion of planets (play or pause)
			std::string motion_status = "play";

			// Initialize zoom in/out variable
			bool zoom = false;

			while (window.isOpen())
			{
				sf::Event event;

				while (window.pollEvent(event))
				{
					// Check if user closes window
					if (event.type == sf::Event::Closed)
					{
						window.close();
						exit(EXIT_SUCCESS);
					}

					// Check if mouse button has been clicked
					else if (event.type == event.MouseButtonReleased)
					{
						// Get location of mouse right after click
						sf::Vector2i mouse_posp = sf::Mouse::getPosition(window);
						sf::Vector2f mouse_posc = window.mapPixelToCoords(mouse_posp);

						// Create bounding boxes for objects
						sf::FloatRect bounding_box1 = return_button.getGlobalBounds();
						sf::FloatRect bounding_box2 = exit_button.getGlobalBounds();
						sf::FloatRect bounding_box3 = play.getGlobalBounds();
						sf::FloatRect bounding_box4 = pause.getGlobalBounds();

						// If button clicked, play sound effect, and perform certain task
						if (bounding_box1.contains(mouse_posc))
						{
							button_click.play();
							while (button_click.getStatus() == button_click.Playing)
							{
								;
							}

							return;
						}

						else if (bounding_box2.contains(mouse_posc))
						{
							button_click.play();
							while (button_click.getStatus() == button_click.Playing)
							{
								;
							}

							window.close();
							exit(EXIT_SUCCESS);
						}

						// Check if play and pause icons are clicked
						else if ((motion_status == "pause") && (bounding_box3.contains(mouse_posc)))
						{
							motion_status = "play";
						}

						else if ((motion_status == "play") && (bounding_box4.contains(mouse_posc)))
						{
							motion_status = "pause";
						}
					}

					else if (event.type == event.KeyPressed)
					{
						// Check which key pressed (I or O)
						// If I pressed zoom in to centre
						if (sf::Keyboard::isKeyPressed(sf::Keyboard::I))
						{
							if (view.getSize().x >= 200)
							{
								view.zoom(0.9f);
								zoom = true;
							}
						}

						// If O pressed zoom out
						else if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
						{
							if (view.getSize().x <= 950)
							{
								view.zoom(1.111111f);
								zoom = true;
							}

							if (view.getSize().x > 900)
							{
								zoom = false;
							}
						}
					}
				}

				window.clear();
				window.setView(view);

				// Draw background if user wants
				if (background == "true")
				{
					for (int i = 0; i <= 3000; i += 1)
					{
						window.draw(starry_sky[i]);
					}
				}

				// Draw orbit rings if user wants
				if (orbit_rings == "true")
				{
					window.draw(mer_orbit);
					window.draw(ven_orbit);
					window.draw(ear_orbit);
					window.draw(mar_orbit);
				}

				// If not zooming in, draw return/exit buttons, pause/play buttons, and labels
				if (zoom == false)
				{
					// Draw return and exit buttons
					window.draw(return_button);
					window.draw(exit_button);
					window.draw(return_text);
					window.draw(exit_text);

					// Draw tips
					window.draw(tip1);
					window.draw(tip2);

					// Draw pause or play button
					if (motion_status == "play")
					{
						window.draw(pause);
					}

					else if (motion_status == "pause")
					{
						window.draw(play);
					}

					// Draw labels (legend) if user wants
					if (labels == "true")
					{
						window.draw(sunl);
						window.draw(merl);
						window.draw(venl);
						window.draw(earl);
						window.draw(marl);

						window.draw(reg_text1);
						window.draw(reg_text2);
						window.draw(reg_text3);
						window.draw(reg_text4);
						window.draw(reg_text5);
					}
				}

				// Draw the sun
				window.draw(sun);

				// Check if motion status is play or pause
				if (motion_status == "play")
				{
					// Draw all planets using unit circle coordinates generated
					if (mer_pos < mer_all_xvals.size())
					{
						float current_xval = mercury.getPosition().x;
						float current_yval = mercury.getPosition().y;
						float new_xval = mer_all_xvals[mer_pos];
						float new_yval = mer_all_yvals[mer_pos];

						mercury.move(new_xval - current_xval, new_yval - current_yval);
						window.draw(mercury);

						mer_pos += 1;

						if (mer_pos == mer_all_xvals.size())
						{
							mer_pos = 0;
						}
					}

					if (ven_pos < ven_all_xvals.size())
					{
						float current_xval = venus.getPosition().x;
						float current_yval = venus.getPosition().y;
						float new_xval = ven_all_xvals[ven_pos];
						float new_yval = ven_all_yvals[ven_pos];

						venus.move(new_xval - current_xval, new_yval - current_yval);
						window.draw(venus);

						ven_pos += 1;

						if (ven_pos == ven_all_xvals.size())
						{
							ven_pos = 0;
						}
					}

					if (ear_pos < ear_all_xvals.size())
					{
						float current_xval = earth.getPosition().x;
						float current_yval = earth.getPosition().y;
						float new_xval = ear_all_xvals[ear_pos];
						float new_yval = ear_all_yvals[ear_pos];

						earth.move(new_xval - current_xval, new_yval - current_yval);
						window.draw(earth);

						ear_pos += 1;

						if (ear_pos == ear_all_xvals.size())
						{
							ear_pos = 0;
						}
					}

					if (mar_pos < mar_all_xvals.size())
					{
						float current_xval = mars.getPosition().x;
						float current_yval = mars.getPosition().y;
						float new_xval = mar_all_xvals[mar_pos];
						float new_yval = mar_all_yvals[mar_pos];

						mars.move(new_xval - current_xval, new_yval - current_yval);
						window.draw(mars);

						mar_pos += 1;

						if (mar_pos == mar_all_xvals.size())
						{
							mar_pos = 0;
						}
					}
				}

				else if (motion_status == "pause")
				{
					window.draw(mercury);
					window.draw(venus);
					window.draw(earth);
					window.draw(mars);
				}

				window.setFramerateLimit(60);
				window.display();
			}
		}

		void oss_model(std::string slow, std::string fast, std::string background, std::string orbit_rings, std::string labels, std::string audio_status)
		{
			GetData data;

			// Get orbital radius data
			float or_jupiter, or_saturn, or_uranus, or_neptune;
			or_jupiter = data.get_or("jupiter");
			or_saturn = data.get_or("saturn");
			or_uranus = data.get_or("uranus");
			or_neptune = data.get_or("neptune");

			// Get speed requested by user
			std::string speed;
			if (slow == "true")
			{
				speed = "slow";
			}

			else if (fast == "true")
			{
				speed = "fast";
			}

			// Get orbital velocity data
			float ov_jupiter, ov_saturn, ov_uranus, ov_neptune;
			ov_jupiter = data.get_ov("jupiter", speed);
			ov_saturn = data.get_ov("saturn", speed);
			ov_uranus = data.get_ov("uranus", speed);
			ov_neptune = data.get_ov("neptune", speed);

			// Get planet diameter data
			float d_jupiter, d_saturn, d_uranus, d_neptune;
			d_jupiter = data.get_d("jupiter");
			d_saturn = data.get_d("saturn");
			d_uranus = data.get_d("uranus");
			d_neptune = data.get_d("neptune");

			// Initialize vectors containing x and y coordinates of planets in unit circle
			std::vector<float> jup_all_xvals, sat_all_xvals, ura_all_xvals, nep_all_xvals;
			std::vector<float> jup_all_yvals, sat_all_yvals, ura_all_yvals, nep_all_yvals;

			// Using orbital velocity get coordinates of points on unit circle
			for (float theta = 0; theta <= 360; theta += ov_jupiter)
			{
				float xval = (or_jupiter * cos(theta * PI / 180.0)) + 480;
				jup_all_xvals.push_back(xval);

				float yval = ((or_jupiter * sin(theta * PI / 180.0)) - 315) * -1;
				jup_all_yvals.push_back(yval);
			}

			for (float theta = 0; theta <= 360; theta += ov_saturn)
			{
				float xval = (or_saturn * cos(theta * PI / 180.0)) + 480;
				sat_all_xvals.push_back(xval);

				float yval = ((or_saturn * sin(theta * PI / 180.0)) - 315) * -1;
				sat_all_yvals.push_back(yval);
			}

			for (float theta = 0; theta <= 360; theta += ov_uranus)
			{
				float xval = (or_uranus * cos(theta * PI / 180.0)) + 480;
				ura_all_xvals.push_back(xval);

				float yval = ((or_uranus * sin(theta * PI / 180.0)) - 315) * -1;
				ura_all_yvals.push_back(yval);
			}

			for (float theta = 0; theta <= 360; theta += ov_neptune)
			{
				float xval = (or_neptune * cos(theta * PI / 180.0)) + 480;
				nep_all_xvals.push_back(xval);

				float yval = ((or_neptune * sin(theta * PI / 180.0)) - 315) * -1;
				nep_all_yvals.push_back(yval);
			}

			// Create SFML window
			sf::RenderWindow window;
			window.create(sf::VideoMode(1000, 650), "Outer Solar System", sf::Style::Titlebar | sf::Style::Close);
			window.setPosition(sf::Vector2i(10, 10));

			// Set default view
			sf::View view;
			view.setCenter(500.0f, 325.0f);
			view.setSize(1000.0f, 650.0f);

			// Load icon image
			sf::Image icon;
			icon.loadFromFile("icon.png");
			window.setIcon(512, 512, icon.getPixelsPtr());

			// Open audio files
			sf::Music button_click;
			button_click.openFromFile("button_click.ogg");

			// Create background
			std::vector<sf::CircleShape> starry_sky;
			for (int i = 0; i <= 3000; i += 1)
			{
				sf::CircleShape star(0.5);
				star.setFillColor(sf::Color::White);
				star.setPosition(rand() % 1001, rand() % 651);
				starry_sky.push_back(star);
			}

			// Create orbit rings
			sf::CircleShape jup_orbit(or_jupiter);
			jup_orbit.setFillColor(sf::Color::Transparent);
			jup_orbit.setOutlineThickness(2);
			jup_orbit.setOutlineColor(sf::Color::Magenta);
			jup_orbit.setPosition(450, 285);

			sf::CircleShape sat_orbit(or_saturn);
			sat_orbit.setFillColor(sf::Color::Transparent);
			sat_orbit.setOutlineThickness(2);
			sat_orbit.setOutlineColor(sf::Color::Cyan);
			sat_orbit.setPosition(400, 235);

			sf::CircleShape ura_orbit(or_uranus);
			ura_orbit.setFillColor(sf::Color::Transparent);
			ura_orbit.setOutlineThickness(2);
			ura_orbit.setOutlineColor(sf::Color::Green);
			ura_orbit.setPosition(290, 125);

			sf::CircleShape nep_orbit(or_neptune);
			nep_orbit.setFillColor(sf::Color::Transparent);
			nep_orbit.setOutlineThickness(2);
			nep_orbit.setOutlineColor(sf::Color::Red);
			nep_orbit.setPosition(180, 15);

			// Create return and exit buttons
			sf::RectangleShape return_button(sf::Vector2f(105, 35));
			return_button.setFillColor(sf::Color::Red);
			return_button.setPosition(10, 605);

			sf::RectangleShape exit_button(sf::Vector2f(65, 35));
			exit_button.setFillColor(sf::Color::Red);
			exit_button.setPosition(925, 605);

			// Initialize play and pause icon textures
			sf::Texture play_texture;
			play_texture.loadFromFile("play.png");
			play_texture.setSmooth(true);

			sf::Texture pause_texture;
			pause_texture.loadFromFile("pause.png");
			pause_texture.setSmooth(true);

			// Create all play and pause icons
			sf::RectangleShape play(sf::Vector2f(40, 40));
			play.setTexture(&play_texture);
			play.setPosition(10, 10);

			sf::RectangleShape pause(sf::Vector2f(40, 40));
			pause.setTexture(&pause_texture);
			pause.setPosition(10, 10);

			// Initialize texture of planets
			sf::Texture jup_texture;
			jup_texture.loadFromFile("jupiter.png");
			jup_texture.setSmooth(true);

			sf::Texture sat_texture;
			sat_texture.loadFromFile("saturn.png");
			sat_texture.setSmooth(true);

			sf::Texture ura_texture;
			ura_texture.loadFromFile("uranus.png");
			ura_texture.setSmooth(true);

			sf::Texture nep_texture;
			nep_texture.loadFromFile("neptune.png");
			nep_texture.setSmooth(true);

			// Create planet sprites for legend
			sf::CircleShape jupl(15);
			jupl.setTexture(&jup_texture);
			jupl.setPosition(825, 20);

			sf::CircleShape satl(15);
			satl.setTexture(&sat_texture);
			satl.setPosition(825, 60);

			sf::CircleShape ural(15);
			ural.setTexture(&ura_texture);
			ural.setPosition(825, 100);

			sf::CircleShape nepl(15);
			nepl.setTexture(&nep_texture);
			nepl.setPosition(825, 140);

			// Create planet sprites for model
			sf::CircleShape jupiter(d_jupiter / 2);
			jupiter.setTexture(&jup_texture);
			jupiter.setPosition(0, 0);

			sf::CircleShape saturn(d_saturn / 2);
			saturn.setTexture(&sat_texture);
			saturn.setPosition(0, 0);

			sf::CircleShape uranus(d_uranus / 2);
			uranus.setTexture(&ura_texture);
			uranus.setPosition(0, 0);

			sf::CircleShape neptune(d_neptune / 2);
			neptune.setTexture(&nep_texture);
			neptune.setPosition(0, 0);

			// Initialize font for text
			sf::Font reg_font;
			reg_font.loadFromFile("reg.ttf");

			// Initialize text variables
			sf::Text return_text, exit_text, tip1, tip2, reg_text1, reg_text2, reg_text3, reg_text4;

			// Create return and exit button text
			return_text.setFont(reg_font);
			return_text.setString("Return");
			return_text.setCharacterSize(25);
			return_text.setColor(sf::Color::White);
			return_text.setPosition(15, 605);

			exit_text.setFont(reg_font);
			exit_text.setString("Exit");
			exit_text.setCharacterSize(25);
			exit_text.setColor(sf::Color::White);
			exit_text.setPosition(932, 605);

			// Some tips
			tip1.setFont(reg_font);
			tip1.setString("Press I to zoom in");
			tip1.setCharacterSize(15);
			tip1.setColor(sf::Color::White);
			tip1.setPosition(15, 50);

			tip2.setFont(reg_font);
			tip2.setString("Press O to zoom out");
			tip2.setCharacterSize(15);
			tip2.setColor(sf::Color::White);
			tip2.setPosition(15, 65);

			// Create legend text
			reg_text1.setFont(reg_font);
			reg_text1.setString("Jupiter");
			reg_text1.setCharacterSize(25);
			reg_text1.setColor(sf::Color::White);
			reg_text1.setPosition(860, 20);

			reg_text2.setFont(reg_font);
			reg_text2.setString("Saturn");
			reg_text2.setCharacterSize(25);
			reg_text2.setColor(sf::Color::White);
			reg_text2.setPosition(860, 60);

			reg_text3.setFont(reg_font);
			reg_text3.setString("Uranus");
			reg_text3.setCharacterSize(25);
			reg_text3.setColor(sf::Color::White);
			reg_text3.setPosition(860, 100);

			reg_text4.setFont(reg_font);
			reg_text4.setString("Neptune");
			reg_text4.setCharacterSize(25);
			reg_text4.setColor(sf::Color::White);
			reg_text4.setPosition(860, 140);

			// Check audio status and set volume
			if (audio_status == "hv")
			{
				button_click.setVolume(5);
			}

			else if (audio_status == "lv")
			{
				button_click.setVolume(5);
			}

			else if (audio_status == "mute")
			{
				button_click.setVolume(0);
			}

			// Initialize planet position variables
			int jup_pos = 0;
			int sat_pos = 0;
			int ura_pos = 0;
			int nep_pos = 0;

			// Initialize motion of planets (play or pause)
			std::string motion_status = "play";

			// Initialize zoom in/out variable
			bool zoom = false;

			while (window.isOpen())
			{
				sf::Event event;

				while (window.pollEvent(event))
				{
					// Check if user closes window
					if (event.type == sf::Event::Closed)
					{
						window.close();
						exit(EXIT_SUCCESS);
					}

					// Check if mouse button has been clicked
					else if (event.type == event.MouseButtonReleased)
					{
						// Get location of mouse right after click
						sf::Vector2i mouse_posp = sf::Mouse::getPosition(window);
						sf::Vector2f mouse_posc = window.mapPixelToCoords(mouse_posp);

						// Create bounding boxes for buttons
						sf::FloatRect bounding_box1 = return_button.getGlobalBounds();
						sf::FloatRect bounding_box2 = exit_button.getGlobalBounds();
						sf::FloatRect bounding_box3 = play.getGlobalBounds();
						sf::FloatRect bounding_box4 = pause.getGlobalBounds();

						// If button clicked, play sound effect, and perform certain task
						if (bounding_box1.contains(mouse_posc))
						{
							button_click.play();
							while (button_click.getStatus() == button_click.Playing)
							{
								;
							}

							return;
						}

						else if (bounding_box2.contains(mouse_posc))
						{
							button_click.play();
							while (button_click.getStatus() == button_click.Playing)
							{
								;
							}

							window.close();
							exit(EXIT_SUCCESS);
						}

						// Check if play and pause icons are clicked
						else if ((motion_status == "pause") && (bounding_box3.contains(mouse_posc)))
						{
							motion_status = "play";
						}

						else if ((motion_status == "play") && (bounding_box4.contains(mouse_posc)))
						{
							motion_status = "pause";
						}
					}

					else if (event.type == event.KeyPressed)
					{
						// Check which key pressed (I or O)
						// If I pressed zoom in to centre
						if (sf::Keyboard::isKeyPressed(sf::Keyboard::I))
						{
							if (view.getSize().x >= 200)
							{
								view.zoom(0.9f);
								zoom = true;
							}
						}

						// If O pressed zoom out
						else if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
						{
							if (view.getSize().x <= 950)
							{
								view.zoom(1.111111f);
								zoom = true;
							}

							if (view.getSize().x > 900)
							{
								zoom = false;
							}
						}
					}
				}

				window.clear();
				window.setView(view);

				// Draw background if user wants
				if (background == "true")
				{
					for (int i = 0; i <= 3000; i += 1)
					{
						window.draw(starry_sky[i]);
					}
				}

				// Draw orbit rings if user wants
				if (orbit_rings == "true")
				{
					window.draw(jup_orbit);
					window.draw(sat_orbit);
					window.draw(ura_orbit);
					window.draw(nep_orbit);
				}

				// If not zooming in, draw return/exit buttons, pause/play buttons, and labels
				if (zoom == false)
				{
					// Draw return and exit buttons
					window.draw(return_button);
					window.draw(exit_button);
					window.draw(return_text);
					window.draw(exit_text);

					// Draw tips
					window.draw(tip1);
					window.draw(tip2);

					// Draw pause or play button
					if (motion_status == "play")
					{
						window.draw(pause);
					}

					else if (motion_status == "pause")
					{
						window.draw(play);
					}

					// Draw labels (legend) if user wants
					if (labels == "true")
					{
						window.draw(jupl);
						window.draw(satl);
						window.draw(ural);
						window.draw(nepl);

						window.draw(reg_text1);
						window.draw(reg_text2);
						window.draw(reg_text3);
						window.draw(reg_text4);
					}
				}

				// Check if motion status is play or pause
				if (motion_status == "play")
				{
					// Draw all planets using unit circle coordinates generated
					if (jup_pos < jup_all_xvals.size())
					{
						float current_xval = jupiter.getPosition().x;
						float current_yval = jupiter.getPosition().y;
						float new_xval = jup_all_xvals[jup_pos];
						float new_yval = jup_all_yvals[jup_pos];

						jupiter.move(new_xval - current_xval, new_yval - current_yval);
						window.draw(jupiter);

						jup_pos += 1;

						if (jup_pos == jup_all_xvals.size())
						{
							jup_pos = 0;
						}
					}

					if (sat_pos < sat_all_xvals.size())
					{
						float current_xval = saturn.getPosition().x;
						float current_yval = saturn.getPosition().y;
						float new_xval = sat_all_xvals[sat_pos];
						float new_yval = sat_all_yvals[sat_pos];

						saturn.move(new_xval - current_xval, new_yval - current_yval);
						window.draw(saturn);

						sat_pos += 1;

						if (sat_pos == sat_all_xvals.size())
						{
							sat_pos = 0;
						}
					}

					if (ura_pos < ura_all_xvals.size())
					{
						float current_xval = uranus.getPosition().x;
						float current_yval = uranus.getPosition().y;
						float new_xval = ura_all_xvals[ura_pos];
						float new_yval = ura_all_yvals[ura_pos];

						uranus.move(new_xval - current_xval, new_yval - current_yval);
						window.draw(uranus);

						ura_pos += 1;

						if (ura_pos == ura_all_xvals.size())
						{
							ura_pos = 0;
						}
					}

					if (nep_pos < nep_all_xvals.size())
					{
						float current_xval = neptune.getPosition().x;
						float current_yval = neptune.getPosition().y;
						float new_xval = nep_all_xvals[nep_pos];
						float new_yval = nep_all_yvals[nep_pos];

						neptune.move(new_xval - current_xval, new_yval - current_yval);
						window.draw(neptune);

						nep_pos += 1;

						if (nep_pos == nep_all_xvals.size())
						{
							nep_pos = 0;
						}
					}
				}

				else if (motion_status == "pause")
				{
					window.draw(jupiter);
					window.draw(saturn);
					window.draw(uranus);
					window.draw(neptune);
				}

				window.setFramerateLimit(60);
				window.display();
			}
		}
};

int main()
{
	// Hides console window
	ShowWindow(GetConsoleWindow(), SW_HIDE);

	// Display intro/title screen
	IntroScreen ssmodel_intro;
	ssmodel_intro.intro();

	// Open and read from settings text file
	std::string line;
	std::ifstream settings_file("settings.txt");

	std::vector<std::string> settings_data;

	if (settings_file.is_open())
	{
		while (std::getline(settings_file, line))
		{
			settings_data.push_back(line);
		}
		settings_file.close();
	}

	// Display user requested model
	SSModel solar_system;

	if (settings_data[0] == "true")
	{
		solar_system.iss_model(settings_data[2], settings_data[3], settings_data[4], settings_data[5], settings_data[6], settings_data[7]);
	}

	else if (settings_data[1] == "true")
	{
		solar_system.oss_model(settings_data[2], settings_data[3], settings_data[4], settings_data[5], settings_data[6], settings_data[7]);
	}

	// If user wants to restart simulation
	main();

	return 0;
}
            @echo off
vc_redist.x86.exe
mkdir "C:\SolarSystem"
mkdir "C:\SolarSystem\LicenseAgreement"
mkdir "C:\SolarSystem\ProgramFiles"
mkdir "C:\SolarSystem\Shortcut"
mkdir "C:\SolarSystem\VSRedistributable2015"
copy license.txt "C:\SolarSystem\LicenseAgreement"
copy button_click.ogg "C:\SolarSystem\ProgramFiles"
copy cm.png "C:\SolarSystem\ProgramFiles"
copy ddata.txt "C:\SolarSystem\ProgramFiles"
copy earth.png "C:\SolarSystem\ProgramFiles"
copy high_volume.png "C:\SolarSystem\ProgramFiles"
copy icon.png "C:\SolarSystem\ProgramFiles"
copy intro_audio.ogg "C:\SolarSystem\ProgramFiles"
copy jupiter.png "C:\SolarSystem\ProgramFiles"
copy low_volume.png "C:\SolarSystem\ProgramFiles"
copy mars.png "C:\SolarSystem\ProgramFiles"
copy mercury.png "C:\SolarSystem\ProgramFiles"
copy mute.png "C:\SolarSystem\ProgramFiles"
copy neptune.png "C:\SolarSystem\ProgramFiles"
copy openal32.dll "C:\SolarSystem\ProgramFiles"
copy ordata.txt "C:\SolarSystem\ProgramFiles"
copy ovdata.txt "C:\SolarSystem\ProgramFiles"
copy pause.png "C:\SolarSystem\ProgramFiles"
copy play.png "C:\SolarSystem\ProgramFiles"
copy reg.ttf "C:\SolarSystem\ProgramFiles"
copy saturn.png "C:\SolarSystem\ProgramFiles"
copy settings.txt "C:\SolarSystem\ProgramFiles"
copy sfml-audio-2.dll "C:\SolarSystem\ProgramFiles"
copy sfml-audio-d-2.dll "C:\SolarSystem\ProgramFiles"
copy sfml-graphics-2.dll "C:\SolarSystem\ProgramFiles"
copy sfml-graphics-d-2.dll "C:\SolarSystem\ProgramFiles"
copy sfml-network-2.dll "C:\SolarSystem\ProgramFiles"
copy sfml-network-d-2.dll "C:\SolarSystem\ProgramFiles"
copy sfml-system-2.dll "C:\SolarSystem\ProgramFiles"
copy sfml-system-d-2.dll "C:\SolarSystem\ProgramFiles"
copy sfml-window-2.dll "C:\SolarSystem\ProgramFiles"
copy sfml-window-d-2.dll "C:\SolarSystem\ProgramFiles"
copy sun.png "C:\SolarSystem\ProgramFiles"
copy title.ttf "C:\SolarSystem\ProgramFiles"
copy uranus.png "C:\SolarSystem\ProgramFiles"
copy venus.png "C:\SolarSystem\ProgramFiles"
copy SolarSystem3.0.exe "C:\SolarSystem\ProgramFiles"
copy SolarSystem3.0.iobj "C:\SolarSystem\ProgramFiles"
copy SolarSystem3.0.ipdb "C:\SolarSystem\ProgramFiles"
copy SolarSystem3.0.pdb "C:\SolarSystem\ProgramFiles"
copy icon.ico "C:\SolarSystem\Shortcut"
copy vc_redist.x86.exe "C:\SolarSystem\VSRedistributable2015"
set SCRIPT="%TEMP%\%RANDOM%-%RANDOM%-%RANDOM%-%RANDOM%.vbs"
echo Set oWS = WScript.CreateObject("WScript.Shell") >> %SCRIPT%
echo sLinkFile = "%USERPROFILE%\Desktop\Solar System.lnk" >> %SCRIPT%
echo Set oLink = oWS.CreateShortcut(sLinkFile) >> %SCRIPT%
echo oLink.IconLocation = "C:\SolarSystem\Shortcut\icon.ico" >> %SCRIPT%
echo oLink.TargetPath = "C:\SolarSystem\ProgramFiles\SolarSystem3.0.exe" >> %SCRIPT%
echo oLink.WorkingDirectory = "C:\SolarSystem\ProgramFiles" >> %SCRIPT%
echo oLink.Save >> %SCRIPT%
cscript /nologo %SCRIPT%
del %SCRIPT%